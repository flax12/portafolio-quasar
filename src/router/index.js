import Vue from 'vue'
import VueRouter from 'vue-router'

//import metodosApi from '../services/metodos-api'

import routes from './routes'

Vue.use(VueRouter)

/*function isAuthenticated() {
  metodosApi.ValidarToken(window.localStorage.token)
  .then(function (response) {
      if(response.data.code != 403){
        console.log("funtcion")
        return true
      }
      return false
  })
  .catch(function (error) {
      return false
  });

}*/

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })



  /*Router.beforeEach((to, from, next) => {
    
    if (!to.meta.isPublic) {

      console.log("entro aqui");

      metodosApi.ValidarToken(window.localStorage.token)
      .then(function (response) {

        //console.log(response)
          if(response.data.code == 403){
            return next({ path: '/' })
          }

          return next();
          
      })
      .catch(function (error) {
        return next({ path: '/' })
      });
      
    }

    if (to.path === '/' && isAuthenticated()) {
      return next({ path: '/Panel' })
    }

    return next();

  })*/

  return Router
}
