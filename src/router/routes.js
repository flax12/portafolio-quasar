const routes = [
  {
    path: "/",
    component: () => import("layouts/LoginLayout.vue"),
    children: [
      { 
        path: '', 
        component: () => import('pages/Login.vue'), 
        meta: {
          isPublic: true
        }
      }
    ]
  },

  {
    path: "/Panel",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { 
        path: '',
        component: () => import('pages/Index.vue'),
        meta: {
            isPublic: false
        } 
      },
      { 
        path: 'Registro-portafolios',
        component: () => import('pages/RegistroPortafolio.vue'),
        meta: {
          isPublic: false
        }  
      },
      { 
        path: 'Usuarios', 
        component: () => import('pages/Usuarios.vue'),
        meta: {
           isPublic: false
         }  
      },
      { path: 'Perfil',
       component: () => import('pages/Perfil.vue'),
       meta: {
          isPublic: false
        } 
      
      }

    ]
  },
  

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;
