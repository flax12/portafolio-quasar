import axios from 'axios'

const apiAxios = axios.create({
  baseURL: 'http://localhost/portafolio-api-rest/'
})

export default apiAxios