import apiAxios from "./instancia-axios"


const metodosApi = {}


metodosApi.ValidarToken = function(token){

  return apiAxios({
        method: 'GET',
        url : '/Auth/validarToken',
        headers : {
          'Authorization' : token
        }
      }).then( (response) => {
      
        return response;
 
      }).catch( (error) => {

        return error
         
      });
}


metodosApi.login = function(data){

  return apiAxios({
        method: 'POST',
        url : '/Auth/login',
        data: data,
        headers : {
          'Content-Type' : "application/json"
        }
      }).then( (response) => {
      
        return response;
 
      }).catch( (error) => {

        return error
         
      });
}

metodosApi.registarPortafolio = function (data,token){

  return apiAxios({
        method: 'post',
        data: data,
        url: '/registrar-portafolio',
        headers : {
          'Content-Type' : "multipart/form-data",
          'Authorization' : token
        }
      })
      .then(function (response) {
          return response
      })
      .catch(function (error) {
          return error
      });
}

metodosApi.getPortafolios = function (token){

  return apiAxios({
        method: 'get',
        url: '/get-portafolio',
        headers: {
          'Authorization' : token
        }
      })
      .then(function (response) {
          return response
      })
      .catch(function (error) {
          return error
      });
}

metodosApi.getUsuarios = function (token){

  return apiAxios({
        method: 'get',
        url: '/data-usuarios',
        headers : {
          'Authorization' : token
        }
      })
      .then(function (response) {
          return response
      })
      .catch(function (error) {
          return error
      });
}

metodosApi.insertUsuario = function (data,token){

  return apiAxios({
        data:data,
        method: 'post',
        url: '/registrar-usuario',
        headers : {
          'Content-Type' : "application/json",
          'Authorization' : token
        }
      })
      .then(function (response) {
          return response
      })
      .catch(function (error) {
          return error
      });
}


export default metodosApi