import metodosApi from "./../../services/metodos-api"

/*
export function someAction (context) {
}
*/

export function login( {commit}, data ){

    return new Promise((resolve, reject) => {
        
        localStorage.removeItem('token')

        return metodosApi.login(data)
        .then(function (response) {
            
            localStorage.setItem('token', response.data.token)
            commit( 'login_mutation' , response.data.token )
            resolve(response)
        })
        .catch(function (error) {
            reject(error)
        });
    });
    
}

export function cerrarSesion({commit})  {
    localStorage.removeItem('token')
    commit('logout')
}
